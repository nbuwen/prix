package prix.generator

import prix.lib.*


interface ByteCodeWriter {
    fun write(code: ByteCode, vararg arguments: Int)
    fun addConstant(code: LiteralType, representation: String)
}

class DebugWriter : ByteCodeWriter {
    data class Line(val code: ByteCode, val args: List<Int>)
    data class Const(val code: LiteralType, val representation: String)

    private val constants = mutableListOf<Const>()
    private val codes = mutableListOf<Line>()

    private val variables = mutableListOf(builtinVariables)

    override fun write(code: ByteCode, vararg arguments: Int) {
        codes.add(Line(code, arguments.asList()))
    }

    override fun addConstant(code: LiteralType, representation: String) {
        constants.add(Const(code, representation))
    }

    fun report() {
        println("Constants:")
        val maxLen = constants.size.toString().length
        constants.forEachIndexed { index, const ->
            val padded = "%${maxLen}d".format(index)
            println("  $padded: ${const.representation}")
        }
        println()
        println("Codes")
        codes.forEach {
            val args = it.args.joinToString { it.toString() }
            println("  ${it.code} $args")
        }
    }

    private fun tryCall(args: Int) {
        val obj = Stack.pop()
        when (obj) {
            is PrixFunction -> obj.invoke(args)
            else -> Exception("Object $obj is not callable!")
        }


    }

    private fun handleConst(index: Int) = with(constants[index]) {
        val constant = when(code) {
            LiteralType.Integer -> PrixInteger(representation.toInt())
            LiteralType.Float -> PrixFloat(representation.toDouble())

            else -> throw Exception("Not implemented")
        }

        Stack.push(constant)
    }

    private fun handleOp(op: Int) {
        val receiver = Stack.peek(2)
        val symbol = OperatorCode.from(op).symbol
        val func = receiver.attr.getOrElse(symbol) {
            receiver.prixClass.attr.getOrElse(symbol) {
                throw Exception("Object $receiver has not operator $symbol")
            }
        }

        Stack.push(func)
        tryCall(2)
    }

    private fun handleStore(level: Int, index: Int) {
        val layer = variables[level]
        while (layer.size <= index) {
            layer.add(PrixNull.singleton)
        }
        layer[index] = Stack.pop()
    }

    private fun handleLoad(level: Int, index: Int) {
        Stack.push(variables[level][index])
    }

    private fun handleCall(args: Int) {
        Stack.reverse(args + 1)

        tryCall(args)
    }

    fun run() {
        println("Running")
        println("-------")
        println()
        codes.forEach {
            when(it.code) {
                ByteCode.Const -> handleConst(it.args[0])
                ByteCode.Op -> handleOp(it.args[0])
                ByteCode.Store -> handleStore(it.args[0], it.args[1])
                ByteCode.Load -> handleLoad(it.args[0], it.args[1])
                ByteCode.Call -> handleCall(it.args[0])

                // else -> throw Exception("ByteCode ${it.code} is not implemented")

            }
        }

        println()
        println("--------")
        println("Finished")

        println()
        println("Variables")
        variables.forEach {
            it.forEachIndexed {
                index, value -> println("$index: $value")
            }
            println()
        }

        println()
        println("Stack: ${Stack.size}")
    }
}