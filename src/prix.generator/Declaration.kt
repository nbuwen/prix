package prix.generator

import org.antlr.v4.runtime.tree.TerminalNode


fun idLocation(id: TerminalNode) =
        "line ${id.symbol.line} ${id.symbol.startIndex}:${id.symbol.stopIndex + 1}"

sealed class Declaration(val const: Boolean, val level: Int, val index: Int) {
    abstract fun pretty(): String
}

class UserDeclaration(private val ctx: PrixParser.DeclarationContext, level: Int, index: Int) : Declaration(ctx.VAR() == null, level, index) {
    override fun pretty(): String {
        val mod = ctx.CONST() ?: ctx.VAR()

        val modName = mod.text
        val line = mod.symbol.line
        val start = mod.symbol.startIndex
        val stop = ctx.ID().symbol.stopIndex
        val name = ctx.ID().text

        return "$modName $name in line $line $start:$stop"
    }
}
class Builtin(private val name: String, index: Int) : Declaration(false, 0, index) {
    override fun pretty(): String {
        return "builtin $name"
    }
}

sealed class NameError(message: String) : Exception(message)

class DuplicateDeclaration(old: Declaration, new: UserDeclaration)
    : NameError("Cannot redeclare ${new.pretty()} previously defined as ${old.pretty()}")
class MissingDeclaration(assignment: PrixParser.AssignmentContext)
    : NameError("Cannot assign to undeclared var ${assignment.ID().text} in ${idLocation(assignment.ID())}")
class ConstantError(assignment: PrixParser.AssignmentContext)
    : NameError("Cannot assign to const ${assignment.ID().text} in line ${idLocation(assignment.ID())}")
class LookupError(lookup: PrixParser.LookupContext)
    : NameError("Cannot lookup undeclared name ${lookup.ID().text} used in line ${idLocation(lookup.ID())}")
