package prix.generator

import PrixParserListener
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.TerminalNode

abstract class Listener : PrixParserListener {
    final override fun enterModule(ctx: PrixParser.ModuleContext?) {}
    final override fun enterAssignment(ctx: PrixParser.AssignmentContext?) {}
    final override fun enterCall(ctx: PrixParser.CallContext?) {}
    final override fun enterCall_with_args(ctx: PrixParser.Call_with_argsContext?) {}
    final override fun enterCall_with_nothing(ctx: PrixParser.Call_with_nothingContext?) {}
    final override fun enterDeclaration(ctx: PrixParser.DeclarationContext?) {}
    final override fun enterExpression(ctx: PrixParser.ExpressionContext?) {}
    final override fun enterFor_loop(ctx: PrixParser.For_loopContext?) {}
    final override fun enterFunction(ctx: PrixParser.FunctionContext?) {}
    final override fun enterIf_block(ctx: PrixParser.If_blockContext?) {}
    final override fun enterIf_else_block(ctx: PrixParser.If_else_blockContext?) {}
    final override fun enterLiteral(ctx: PrixParser.LiteralContext?) {}
    final override fun enterLookup(ctx: PrixParser.LookupContext?) {}
    final override fun enterMember_access(ctx: PrixParser.Member_accessContext?) {}
    final override fun enterOperation(ctx: PrixParser.OperationContext?) {}
    final override fun enterStatement(ctx: PrixParser.StatementContext?) {}
    final override fun enterWhile_loop(ctx: PrixParser.While_loopContext?) {}

    open fun visitAssignment(ctx: PrixParser.AssignmentContext) { throw NotImplementedError("assignment") }
    open fun visitCall(ctx: PrixParser.CallContext) { throw NotImplementedError("call") }
    open fun visitCallWithArgs(ctx: PrixParser.Call_with_argsContext) { throw NotImplementedError("call with args") }
    open fun visitCallWithNoting(ctx: PrixParser.Call_with_nothingContext) { throw NotImplementedError("call with nothing") }
    open fun visitDeclaration(ctx: PrixParser.DeclarationContext) { throw NotImplementedError("declaration") }
    open fun visitExpression(ctx: PrixParser.ExpressionContext) { throw NotImplementedError("expression") }
    open fun visitFor(ctx: PrixParser.For_loopContext) { throw NotImplementedError("for") }
    open fun visitFunction(ctx: PrixParser.FunctionContext) { throw NotImplementedError("function") }
    open fun visitIf(ctx: PrixParser.If_blockContext) { throw NotImplementedError("if") }    
    open fun visitIfElse(ctx: PrixParser.If_else_blockContext) { throw NotImplementedError("if else") } 
    open fun visitLiteral(ctx: PrixParser.LiteralContext) { throw NotImplementedError("literal") } 
    open fun visitLookup(ctx: PrixParser.LookupContext) { throw NotImplementedError("lookup") }    
    open fun visitMemberAccess(ctx: PrixParser.Member_accessContext) { throw NotImplementedError("member access") }    
    open fun visitModule(ctx: PrixParser.ModuleContext) { throw NotImplementedError("module") }
    open fun visitOperation(ctx: PrixParser.OperationContext) { throw NotImplementedError("operation") }
    open fun visitStatement(ctx: PrixParser.StatementContext) { throw NotImplementedError("statement") }
    open fun visitWhile(ctx: PrixParser.While_loopContext) { throw NotImplementedError("while") }
    

    final override fun exitAssignment(ctx: PrixParser.AssignmentContext?) = visitAssignment(ctx!!)
    final override fun exitCall(ctx: PrixParser.CallContext?) = visitCall(ctx!!)
    final override fun exitCall_with_args(ctx: PrixParser.Call_with_argsContext?) = visitCallWithArgs(ctx!!)
    final override fun exitCall_with_nothing(ctx: PrixParser.Call_with_nothingContext?) = visitCallWithNoting(ctx!!)
    final override fun exitDeclaration(ctx: PrixParser.DeclarationContext?) = visitDeclaration(ctx!!)
    final override fun exitExpression(ctx: PrixParser.ExpressionContext?) = visitExpression(ctx!!)
    final override fun exitFor_loop(ctx: PrixParser.For_loopContext?) = visitFor(ctx!!)
    final override fun exitFunction(ctx: PrixParser.FunctionContext?) = visitFunction(ctx!!)
    final override fun exitIf_block(ctx: PrixParser.If_blockContext?) = visitIf(ctx!!)
    final override fun exitIf_else_block(ctx: PrixParser.If_else_blockContext?) = visitIfElse(ctx!!)
    final override fun exitLiteral(ctx: PrixParser.LiteralContext?) = visitLiteral(ctx!!)
    final override fun exitLookup(ctx: PrixParser.LookupContext?) = visitLookup(ctx!!)
    final override fun exitMember_access(ctx: PrixParser.Member_accessContext?) = visitMemberAccess(ctx!!)
    final override fun exitModule(ctx: PrixParser.ModuleContext?) = visitModule(ctx!!)
    final override fun exitOperation(ctx: PrixParser.OperationContext?) = visitOperation(ctx!!)
    final override fun exitStatement(ctx: PrixParser.StatementContext?) = visitStatement(ctx!!)
    final override fun exitWhile_loop(ctx: PrixParser.While_loopContext?) = visitWhile(ctx!!)

    final override fun exitEveryRule(p0: ParserRuleContext?) {}
    final override fun enterEveryRule(p0: ParserRuleContext?) {}

    final override fun visitErrorNode(p0: ErrorNode?) {}
    final override fun visitTerminal(p0: TerminalNode?) {}
}