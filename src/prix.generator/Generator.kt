package prix.generator

import org.antlr.v4.runtime.*

import PrixParser
import PrixLexer
import org.antlr.v4.gui.TreeViewer
import prix.lib.builtinDeclarations


fun canonicalStringRepresentation(string: String): String {
    println("clean $string")
    var clean = if (string.startsWith("'''") || string.startsWith("\"\"\"")) {
        string.substring(3, string.length - 3)
    } else {
        string.substring(1, string.length - 1)
    }
    clean = clean.replace("""(?<!\\)"""".toRegex(), "\"")
    clean = clean.replace("""(?<!\\)\\'""".toRegex(), "'")

    return "\"$clean\""
}


class Generator(private val writer: ByteCodeWriter) : Listener() {
    private val constants = mutableMapOf<String, Int>()
    private val nameStack = mutableListOf(builtinDeclarations)
    private var currentNamespace = nameStack.last()

    private fun push() {
        currentNamespace = mutableMapOf()
        nameStack.add(currentNamespace)
    }

    private fun pop() {
        currentNamespace = nameStack.removeAt(nameStack.size - 1)
    }

    private fun makeDeclaration(ctx: PrixParser.DeclarationContext)
            = UserDeclaration(ctx, nameStack.size - 1, currentNamespace.size)

    private fun declare(ctx: PrixParser.DeclarationContext): Declaration {
        val name = ctx.ID().text

        val declaration = makeDeclaration(ctx)
        currentNamespace[name]?.let {
            throw DuplicateDeclaration(it, declaration)
        }

        currentNamespace[name] = declaration
        return currentNamespace[name]!!
    }

    private fun assertDeclared(ctx: PrixParser.LookupContext): Declaration {
        currentNamespace[ctx.ID().text]?.let {
            return it
        }
        throw LookupError(ctx)
    }

    private fun assertVar(ctx: PrixParser.AssignmentContext): Declaration {
        currentNamespace[ctx.ID().text]?.let {
            if (it.const) {
                throw ConstantError(ctx)
            }

            return it
        } ?: throw MissingDeclaration(ctx)
    }

    override fun visitDeclaration(ctx: PrixParser.DeclarationContext) {
        val declaration = declare(ctx)
        writer.write(ByteCode.Store, declaration.level, declaration.index)
    }

    override fun visitAssignment(ctx: PrixParser.AssignmentContext) {
        val declaration = assertVar(ctx)
        writer.write(ByteCode.Store, declaration.level, declaration.index)
    }

    override fun visitLookup(ctx: PrixParser.LookupContext) {
        val declaration = assertDeclared(ctx)
        writer.write(ByteCode.Load, declaration.level, declaration.index)
    }

    override fun visitOperation(ctx: PrixParser.OperationContext) {
        val code = OperatorCode.from(ctx.OPERATOR()!!.text)
        writer.write(ByteCode.Op, code.ordinal)
    }

    override fun visitLiteral(ctx: PrixParser.LiteralContext) {
        var type = LiteralType.String
        var text = ""

        ctx.BOOL_LITERAL()?.let {
            type = LiteralType.Bool
            text = if (it.text == "true") "1" else "0"
        } ?: (
                ctx.DOUBLE_STRING_LITERAL() ?:
                ctx.SINGLE_STRING_LITERAL() ?:
                ctx.TRIPLE_DOUBLE_STRING_LITERAL() ?:
                ctx.TRIPLE_SINGLE_STRING_LITERAL()
        )?.let {
            type = LiteralType.String
            text = canonicalStringRepresentation(it.text)
        } ?: ctx.INTEGER_LITERAL()?.let {
            type = LiteralType.Integer
            text = it.text.removePrefix("+")
        } ?: ctx.FLOAT_LITERAL()?.let {
            type = LiteralType.Float
            text = it.text.removePrefix("+")
        } ?: throw Exception("Unknown literal type in context $ctx")

        val index = constants.getOrPut(text) {
            writer.addConstant(type, text)
            constants.size
        }

        writer.write(ByteCode.Const, index)
    }

    override fun visitCallWithArgs(ctx: PrixParser.Call_with_argsContext) {
        writer.write(ByteCode.Call, ctx.expression().size)
    }

    override fun visitCall(ctx: PrixParser.CallContext) {
        writer.write(ByteCode.Call, 0)
    }

    override fun visitCallWithNoting(ctx: PrixParser.Call_with_nothingContext) {
        writer.write(ByteCode.Call, 0)
    }

    override fun visitExpression(ctx: PrixParser.ExpressionContext) {}
    override fun visitStatement(ctx: PrixParser.StatementContext) {}
    override fun visitModule(ctx: PrixParser.ModuleContext) {}
}


fun main(args: Array<String>) {
    val fileName = if (args.isEmpty()) "samples/simple.px" else args[0]

    try {
        val input = CharStreams.fromFileName(fileName)

        val lexer = PrixLexer(input)
        val tokens = CommonTokenStream(lexer)
        val parser = PrixParser(tokens)

        val debug = DebugWriter()
        parser.addParseListener(Generator(debug))
        val tree = parser.module()
        debug.report()

        debug.run()

        val rules = PrixParser.ruleNames.toList()
        val view = TreeViewer(rules, tree)
        view.open()
    } catch (e: NoSuchFileException) {
        println("File $fileName does not exist")
    } catch (e: NameError) {
        println("Name Error - ${e.javaClass.simpleName}: ${e.message}")
    }
}