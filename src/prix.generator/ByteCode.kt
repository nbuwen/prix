package prix.generator

enum class ByteCode {
    Store,
    Load,
    Const,

    Op,
    Call
}

enum class LiteralType {
    Integer,
    Float,
    String,
    Bool
}

enum class OperatorCode(val symbol: String) {
    Plus("+"),
    Minus("-"),
    Times("*"),
    Divide("/"),
    Mod("%"),
    And("&"),
    Or("|"),
    Eq("="),
    Lt("<"),
    Gt(">"),
    Le("<="),
    Ge(">=");

    companion object {
        private val symbolMap = OperatorCode.values().associateBy { it.symbol }
        private val indexMap = OperatorCode.values().associateBy { it.ordinal }

        fun from(symbol: String) = symbolMap.getOrElse(symbol) { throw Exception("Unknown operator $symbol") }
        fun from(index: Int) = indexMap.getOrElse(index) { throw Exception("Unknown operator $index")}
    }
}