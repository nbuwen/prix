package prix.lib

open class PrixObject(prixClass: PrixObject? = null) {

    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this

        override fun toString() = "Type"
    }

    open val attr = mapOf(
            "class" to (prixClass ?: PrixObject)
    )
    open val prixClass by lazy { (prixClass ?: PrixObject) }

    override fun toString() = """Object of type $prixClass"""


    open fun str() = "Object"
}


typealias Func = (Int) -> Int

open class PrixFunction(private val name: String, private val func: Func) : PrixObject(PrixFunction) {

    override fun toString() = "Function $name"

    fun invoke(args: Int) = func(args)

    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this

        override fun toString() = "FunctionType"
    }

    override fun str() = "Function $name"
}

open class PrixFloat(val value: Double): PrixObject(PrixFloat) {
    override fun toString() = "Float $value"

    companion object : PrixObject() {
        override val attr = mapOf(
                "class" to this,
                "+" to PrixFunction("+") {
                    val right = Stack.pop()
                    val left = Stack.pop()
                    val result = PrixFloat((left as PrixFloat).value + toValue(right))
                    Stack.push(result)
                    1
                }
        )
        override val prixClass = this

        override fun toString() = "FloatType"

        private fun toValue(obj: PrixObject) = when(obj) {
            is PrixFloat -> obj.value
            is PrixInteger -> obj.value.toDouble()

            else -> throw Exception("Cannot convert $obj to Integer")
        }
    }

    override fun str() = value.toString()
}

open class PrixInteger(val value: Int): PrixObject(PrixInteger) {
    override fun toString() = "Integer $value"

    companion object : PrixObject() {
        override val attr = mapOf(
                "class" to this,
                "str" to PrixFunction("str") { Stack.push(PrixString((Stack.pop() as PrixInteger).value.toString())); 1 },
                "+" to PrixFunction("+") {
                    val right = Stack.pop()
                    val left = Stack.pop()
                    val result = PrixInteger((left as PrixInteger).value + toValue(right))
                    Stack.push(result)
                    1
                }
        )
        override val prixClass = this

        override fun toString() = "IntegerType"

        private fun toValue(obj: PrixObject) = when(obj) {
            is PrixInteger -> obj.value
            is PrixFloat -> obj.value.toInt()

            else -> throw Exception("Cannot convert $obj to Integer")
        }
    }

    override fun str() = value.toString()
}

open class PrixString(private val value: String): PrixObject(PrixString) {
    override fun toString() = value

    companion object : PrixObject() {
        override val attr = mapOf(
                "class" to this
        )
        override val prixClass = this

        override fun toString() = "StringType"
    }

    override fun str() = value
}


open class PrixNull : PrixObject(PrixNull) {
    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this
        val singleton = PrixNull()

        override fun toString() = "NullType"
    }

    override fun str() = "null"
}