package prix.lib

import prix.generator.Builtin
import prix.generator.Declaration

fun builtinPrint(args: Int): Int {
    (1 .. args).forEach {

        print("${Stack.pop().str()} ")
    }
    println()
    return 0
}

fun builtinType(args: Int): Int {
    if (args != 1) {
        throw Exception("Unexpected number of arguments to builtint type(). Got $args expected 1")
    }

    Stack.push(Stack.pop().prixClass)
    return 1
}

val builtins = mutableMapOf(
        "print" to PrixFunction("builtin print", ::builtinPrint),
        "type" to PrixFunction("builtin type", ::builtinType)
)

val builtinDeclarations by lazy {
    val dict = mutableMapOf<String, Declaration>()
    var index = 0
    builtins.forEach {
        dict[it.key] = Builtin(it.key, index++)
    }
    dict
}

val builtinVariables by lazy {
    val list = mutableListOf<PrixObject>()

    builtins.forEach {
        list.add(it.value)
    }
    list
}


fun main(args: Array<String>) {

}