package prix.lib

object Stack {
    private val stack = mutableListOf<PrixObject>()

    val size: Int
        get() = stack.size

    fun pop() = stack.removeAt(stack.size - 1)
    fun peek(depth: Int = 1) = stack[stack.size - depth]
    fun push(obj: PrixObject) = stack.add(obj)

    fun reverse(n: Int) {
        val total = stack.size
        (0 until n / 2).forEach {
            val tmp = stack[total - 1 - it]
            stack[total - 1 - it] = stack[total - n + it]
            stack[total - n + it] = tmp
        }
    }
}