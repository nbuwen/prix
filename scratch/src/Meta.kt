open class PrixObject(prixClass: PrixObject? = null) {

    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this

        override fun toString() = "Type"
    }

    open val attr = mapOf(
            "class" to (prixClass ?: PrixObject)
    )
    open val prixClass by lazy { (prixClass ?: PrixObject) }

    override fun toString() = """Object of type $prixClass"""


}


typealias Func = (Int) -> Int

open class PrixFunction(private val func: Func) : PrixObject(PrixFunction) {

    override fun toString() = "Function"

    fun invoke(args: Int) = func(args)

    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this

        override fun toString() = "FunctionType"
    }
}

open class PrixInteger(private val value: Int): PrixObject(PrixInteger) {

    override fun toString() = "Integer $value"

    companion object : PrixObject() {
        override val attr = mapOf(
                "class" to this,
                "+" to PrixFunction {
                    println("add"); 0
                }
        )
        override val prixClass = this

        override fun toString() = "IntegerType"

    }
}

open class PrixNull : PrixObject(PrixNull) {
    companion object : PrixObject() {
        override val attr = mapOf<String, PrixObject>(
                "class" to this
        )
        override val prixClass = this
        val singleton = PrixNull()

        override fun toString() = "NullType"
    }
}


fun main(args: Array<String>) {
    var o = PrixObject()

    println(o.attr["class"])
    println("object $o class ${o.prixClass}")

    o = PrixFunction { println("call"); 0 }
    println("object $o class ${o.prixClass}")
    println("eq ${o.prixClass == PrixObject}")
    o.invoke(0)

    o = PrixInteger(23)
    println("object $o class ${o.prixClass}")
    println("eq ${o.prixClass == PrixObject}")

    println(PrixNull.singleton)
}