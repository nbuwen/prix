from sys import argv, executable
from subprocess import run
from os import environ, getcwd, replace, remove, name as os_name
from os.path import join
from shutil import copy
from glob import glob
from urllib.request import urlopen


ALIAS = 'doskey' if os_name == 'nt' else 'alias'


def _run_with_env(command, where):
    env = environ.copy()
    path = join(getcwd(), 'antlr', 'antlr-4.7.1-complete.jar')
    env['CLASSPATH'] = path

    prefix = f'cd {where} &&'.split()
    suffix = '&& cd ..'.split()

    if run(prefix + command.split() + suffix, env=env, shell=True).returncode != 0:
        raise RuntimeError


def _move_files(*patterns, copy_file=False):
    operation = copy if copy_file else replace
    for file in patterns:
        operation(join('grammar', file), join('generated', file))


def download(blocksize=8192):
    """Download the antlr jar"""
    url = 'http://www.antlr.org/download/antlr-4.7.1-complete.jar'
    target = join('antlr', url.rsplit('/', maxsplit=1)[1])
    http_in = urlopen(url)
    size = int(http_in.info().get('Content-Length'))
    current = blocksize

    with open(target + '.downloading', 'wb') as fout:
        buffer = http_in.read(blocksize)
        while buffer:
            fout.write(buffer)
            buffer = http_in.read(blocksize)

            current += len(buffer)
            print(f"progress: {current:>{len(str(size))}} / {size} ({current / size:3.0%})\r", end="", flush=True)

    replace(target + '.downloading', target)


def lexer():
    """Generate the lexer code"""
    _run_with_env('java org.antlr.v4.Tool PrixLexer.g4', 'grammar')
    _move_files('PrixLexer.java', 'PrixLexer.interp')
    _move_files('PrixLexer.tokens', copy_file=True)


def parser():
    """Generate the parser code"""
    _run_with_env('java org.antlr.v4.Tool PrixParser.g4', 'grammar')
    _move_files('PrixParser.interp', 'PrixParser.tokens', 'PrixParser.java',
                'PrixParserBaseListener.java', 'PrixParserListener.java')


def generate():
    """Generate both lexer and parser code"""
    print("lexer ...")
    lexer()
    print("parser ...")
    parser()


def build():
    _run_with_env('javac PrixLexer.java', 'generated')
    _run_with_env('javac PrixParserListener.java', 'generated')
    _run_with_env('javac PrixParser.java', 'generated')


def clean():
    """Deletes all generated files"""
    for pattern in ('PrixLexer*', 'PrixParser*'):
        for file in glob(join('generated', pattern)):
            remove(file)


def unregister():
    """Unregister shortcuts"""
    for command in COMMANDS:
        run([ALIAS, command, '='])


COMMANDS = {func.__name__: func for func in (lexer, parser, generate, build, download, clean, unregister)}


def _exit_help(message=None):
    commands = '|'.join(COMMANDS.keys())
    print(f"usage: {argv[0]} {commands} [ARGS]")

    if message is not None:
        print("Error:", message)

    for name, func in COMMANDS.items():
        print(f"    {name:<10}: {func.__doc__}")

    print("shortcuts are active")
    print(f"use COMMAND instead of {executable} {argv[0]} COMMAND")


def _register_shortcuts():
    for command in COMMANDS:
        run([ALIAS, command, '=', executable, 'manage.py', command], shell=True)


def main(args=argv):
    _register_shortcuts()

    if len(args) == 1:
        _exit_help()
    else:
        try:
            command = COMMANDS[args[1]]
        except KeyError:
            _exit_help(f"Unknown command '{args[1]}'")
        else:
            try:
                command(*args[2:])
            except TypeError:
                _exit_help(command.__doc__)
            except RuntimeError:
                print("aborting ...")
            print("done")


if __name__ == '__main__':
    main()
