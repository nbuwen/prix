lexer grammar PrixLexer;

CONST : 'const' ;
VAR : 'var' ;
IF : 'if' ;
WHILE : 'while' ;
ELSE : 'else' ;
OF : 'of' ;
WITH : 'with' ;
NOTHING : 'nothing' ;
CALL : 'call' ;
END : 'end' ;
FUN : 'fun' ;
TAKES : 'takes' ;
DO : 'do' ;
RETURN : 'return' ;
FOR : 'for' ;
IN : 'in' ;

ASSIGNMENT_OPERATOR : [+*/%|&-]?'=' ;
OPERATOR : [+*/%|<>&=-] | ([<>!]'=') ;
INTEGER_LITERAL : [+-]?[0-9]+ ;
BOOL_LITERAL : 'true' | 'false' ;
FLOAT_LITERAL : [+-]?[0-9]+[.][0-9]+ ;
TRIPLE_DOUBLE_QUOTE : '"""' -> more,mode(TRIPLE_DOUBLE_STRING) ;
DOUBLE_QUOTE : '"' -> more,mode(DOUBLE_STRING) ;
TRIPLE_SINGLE_QUOTE : '\'\'\'' -> more,mode(TRIPLE_SINGLE_STRING) ;
SINGLE_QUOTE : '\'' -> more,mode(SINGLE_STRING) ;

ID : [a-zA-Z_][a-zA-Z_0-9]* ;

WS : [ \t\r\n]+ -> skip ;

mode TRIPLE_DOUBLE_STRING ;

TRIPLE_DOUBLE_STRING_LITERAL : '"""' -> mode(DEFAULT_MODE) ;
TRIPLE_DOUBLE_STRING_CONTENT : . -> more ;

mode DOUBLE_STRING ;

DOUBLE_STRING_BS : '\\' -> more,pushMode(SKIP_ONE) ;
DOUBLE_STRING_LITERAL : '"' -> mode(DEFAULT_MODE) ;
DOUBLE_STRING_CONTENT : . -> more ;

mode TRIPLE_SINGLE_STRING ;

TRIPLE_SINGLE_STRING_LITERAL : '\'\'\'' -> mode(DEFAULT_MODE) ;
TRIPLE_SINGLE_STRING_CONTENT : . -> more ;

mode SINGLE_STRING ;

SINGLE_STRING_LITERAL : '\'' -> mode(DEFAULT_MODE) ;
SINGLE_STRING_CONTENT : . -> more ;

mode SKIP_ONE ;

ANYTHING : . -> more,popMode ;