parser grammar PrixParser;

options { tokenVocab=PrixLexer; }

module: statement+ ;


statement : expression
    | function
    | while_loop
    | for_loop
    | if_else_block | if_block
    | declaration
    | assignment ;

function : FUN ID TAKES (((ID | assignment)+ DO) | NOTHING) statement* END ;

for_loop : FOR ID IN expression statement+ END ;
while_loop: WHILE expression statement+ END ;
if_block : IF expression statement+ END ;
if_else_block : IF expression statement+ ELSE statement+ END ;
declaration : (CONST | VAR) ID ASSIGNMENT_OPERATOR expression ;
assignment : ID ASSIGNMENT_OPERATOR expression ;

expression : lookup
    | RETURN expression
    | member_access
    | call_with_args
    | call_with_nothing
    | call
    | literal
    | operation ;

member_access : ID OF expression ;
call_with_args : lookup WITH expression+ ;
call_with_nothing : lookup WITH NOTHING ;
call : CALL lookup ;
operation : OPERATOR expression expression ;
literal : INTEGER_LITERAL
    | BOOL_LITERAL
    | FLOAT_LITERAL
    | DOUBLE_STRING_LITERAL
    | SINGLE_STRING_LITERAL
    | TRIPLE_DOUBLE_STRING_LITERAL
    | TRIPLE_SINGLE_STRING_LITERAL ;

lookup : ID ;
