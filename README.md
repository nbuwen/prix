Prix
====

Syntax
------

### Variable Definitions

```
var x = 1
const y = 2
```

### Expressions

#### Literals

```
const integer = 1
const float = 3.1
```

#### Operators

```
# arithmetic operators
const plus = + 1 2
const minus = - 10 3
const times = * 2 3
const divide = / 10 5
const mod = % 10 3

const expression = + * 2 3  / 10 5

# comparison operators
const equals = = 2 2
const gt = > 2 3
const lt = < 3 2
const ge = >= 3 2
const le = <= 2 3
```

#### Calls

```
# arguments and return value
const result = sum with 1 2 3

# arguments
print with "hello"

# no arguments
update with nothing

# no arguments (alternative)
call exit


```

#### Properties

```
const number_of_cats = size of cats
```

### Assignments

```
const value = 2
var increment = value
increment = + increment 1
```

### If Else

```
if = 3 3
    x = 1
end

if = 3 3
    x = 1
else
    x = 2
end
```

### While

```
while = 2 x
    print with "hello"
    x = + 1 x
end
```

### For

```
for i in generator with nothing
    print with i
end
```